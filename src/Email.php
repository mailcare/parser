<?php

namespace MailCare\Parser;

interface Email
{
	public function getFrom(): Address;
	public function getTo(): array;
	public function getSubject(): string;
	public function getText(): string;
	public function getHtml(): string;
	/**
	 * @return Attachment[]
	 */
	public function getAttachments(): array;
}