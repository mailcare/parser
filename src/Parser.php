<?php

namespace MailCare\Parser;

interface Parser
{
	public function parseFromStream($stream): Email;
	public function parseFromText(string $text): Email;
	public function parseFromPath(string $path): Email;
}