<?php

namespace MailCare\Parser;

interface Address
{
	public function getEmail(): string;
	public function getDisplay(): string;
	
}