<?php

namespace MailCare\Parser;

interface Attachment
{
	public function save(string $directory): string;
	public function getFilename(): string;
	
}